mod client;
mod config;
mod server;
mod utils;

use anyhow::Error;
use clap::Parser;
use tracing_subscriber::EnvFilter;
use veilid_core::{CryptoKind, CRYPTO_KIND_VLD0};

const MAX_APP_MESSAGE_MESSAGE_LEN: usize = 32768;
const CRYPTO_KIND: CryptoKind = CRYPTO_KIND_VLD0;

/// Like netcat but with Veilid
#[derive(Parser, Debug)]
struct Args {
    #[arg(short, long)]
    server: bool,

    #[arg(long)]
    client: Option<String>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let args = Args::parse();

    let default_env_filter = EnvFilter::try_from_default_env();
    let fallback_filter = EnvFilter::new("veilid_core=warn,info");
    let env_filter = default_env_filter.unwrap_or(fallback_filter);

    tracing_subscriber::fmt()
        .with_writer(std::io::stderr)
        .with_env_filter(env_filter)
        .init();

    let temp_dir = tempfile::tempdir()?;

    let key_pair = veilid_core::Crypto::generate_keypair(CRYPTO_KIND)?;

    if args.server {
        server::server(temp_dir.into_path(), key_pair).await?;
    } else if let Some(encoded) = args.client {
        use base64::{engine::general_purpose, Engine as _};
        let blob = general_purpose::STANDARD_NO_PAD.decode(encoded).unwrap();
        client::client(temp_dir.into_path(), key_pair, blob).await?;
    }

    Ok(())
}
