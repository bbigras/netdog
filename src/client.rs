use anyhow::{Context, Error};
use flume::{unbounded, Receiver, Sender};
use futures_util::select;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tracing::info;
use veilid_core::tools::*;
use veilid_core::{api_startup, CryptoTyped, KeyPair, VeilidUpdate};

use crate::config::config_callback;
use crate::utils::{wait_for_attached, wait_for_network_start, wait_for_public_internet_ready};
use crate::CRYPTO_KIND;
use crate::MAX_APP_MESSAGE_MESSAGE_LEN;

pub async fn client(
    temp_dir: std::path::PathBuf,
    key_pair: CryptoTyped<KeyPair>,
    blob: Vec<u8>,
) -> Result<(), Error> {
    let mut stdin = tokio::io::stdin();
    let mut stdout = tokio::io::stdout();

    let (sender, receiver): (
        Sender<veilid_core::VeilidUpdate>,
        Receiver<veilid_core::VeilidUpdate>,
    ) = unbounded();

    // Create VeilidCore setup
    let update_callback = Arc::new(move |change: veilid_core::VeilidUpdate| {
        if let Err(e) = sender.send(change) {
            // Don't log here, as that loops the update callback in some cases and will deadlock
            let change = e.into_inner();
            info!("error sending veilid update callback: {:?}", change);
        }
    });

    let temp_dir_clone = temp_dir.clone();
    let api = api_startup(
        update_callback,
        Arc::new(move |key| config_callback(temp_dir_clone.clone(), key_pair.clone(), key)),
    )
    .await
    .expect("startup failed");

    api.attach().await?;

    wait_for_network_start(&api).await;

    wait_for_attached(&api).await;

    let rc = api.routing_context().with_privacy()?;

    wait_for_public_internet_ready(&api).await?;

    let update_receiver_jh = spawn(async move {
        loop {
            select! {
                res = receiver.recv_async() => {
                    if let Ok(change) = res {
                        match change {
                            VeilidUpdate::AppMessage(msg) => {
                                let r = stdout.write(&msg.message()).await.unwrap();
                                if r == 0 {
                                    // TODO: use a channel or something
                                    use std::process;
                                    process::exit(0x0100);
                                }
                            }
                            _ => ()
                        }
                    } else {
                        break;
                    }
                }
            };
        }
    });

    let route_id = api.import_remote_private_route(blob).unwrap();

    info!("creating a private route");
    let (_route_id, blob) = api
        .new_custom_private_route(
            &[CRYPTO_KIND],
            veilid_core::Stability::Reliable,
            veilid_core::Sequencing::EnsureOrdered,
        )
        .await
        .context("new_custom_private_route")?;
    info!("creating a private route, done");

    let target = veilid_core::Target::PrivateRoute(route_id);

    rc.app_message(target.clone(), blob.clone())
        .await
        .context("app_message")?;

    let mut buf = [0; MAX_APP_MESSAGE_MESSAGE_LEN];

    let tx = async {
        info!("sending file");

        loop {
            let len = stdin.read(&mut buf).await?;
            if len == 0 {
                break;
            }
            rc.app_message(target.clone(), buf[..len].to_vec())
                .await
                .context("app_message")?;
        }

        // send empty payload to finish
        rc.app_message(target.clone(), vec![])
            .await
            .context("app_message")?;
        info!("sending file, done");

        Ok::<_, Error>(())
    };

    let rx = async {
        let _ = update_receiver_jh.await;
        Ok::<_, Error>(())
    };

    let (tx_done, rx_done) = tokio::join!(tx, rx);

    api.shutdown().await;

    Ok(())
}
